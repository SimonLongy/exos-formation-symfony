<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Message;
use App\Entity\Topic;
use App\Form\TopicType;
use App\Repository\CategorieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategorieController extends AbstractController
{
    #[Route('/categoriesPrincipales', name: 'app_categories_principales')]
    public function categoriesPrincipales(CategorieRepository $categorieRepository): Response
    {
        $categoriesPrincipales = $categorieRepository->findBy(['parent' => null]);

        return $this->render('categorie/listeCategories.html.twig', [
            'categoriesPrincipales' => $categoriesPrincipales,
        ]);
    }

    #[Route('/categorie/{id}', name: 'app_categorie')]
    public function categorie(Categorie $categorie, CategorieRepository $categorieRepository): Response
    {
        $sousCategories = $categorieRepository->findBy(['parent' => $categorie]);
        $topics = $categorie->getTopics();

        return $this->render('categorie/categorie.html.twig', [
            'categorie' => $categorie,
            'sousCategories' => $sousCategories,
            'topics' => $topics
        ]);
    }

    #[Route('/ajouterTopic/{id}', name: 'app_categorie_ajouter_topic')]
    public function ajouterTopic(Request $request, Categorie $categorie, EntityManagerInterface $em): Response
    {
        // Création d'un topic initialement vierge
        $topic = new Topic();

        // Création d'un objet formulaire pour ajouter un topic
        $formulaireTopic = $this->createForm(TopicType::class, $topic);

        // Récupération des données dans $topic si elles ont été soumises
        $formulaireTopic->handleRequest($request);

        // Traiter les données du formulaire s'il a été soumis et est valide
        if($formulaireTopic->isSubmitted() && $formulaireTopic->isValid())
        {
            $topic->setCategorie($categorie);

            $message = new Message();
            $message->setContenu($formulaireTopic['contenuPremierMessage']->getData());

            $topic->addMessage($message);

            // Les dates de création et de modification sont faites avec l'event PrePersist
            $em->persist($message);
            $em->persist($topic);
            $em->flush();

            // Rediriger l'utilisateur vers la page du topic
            return $this->redirectToRoute('app_topic', ['id' => $topic->getId()]);
        }

        return $this->render('categorie/ajouterTopic.html.twig', [
            'vueFormulaireTopic' => $formulaireTopic->createView()
        ]);
    }
}
