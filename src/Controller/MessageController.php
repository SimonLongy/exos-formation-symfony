<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\MessageType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageController extends AbstractController
{
    #[Route('/message/edit/{id}', name: 'app_message_edit')]
    public function editMessage(Request $request, Message $message, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if($this->getUser() !== $message->getCreateur()) {
            return $this->redirectToRoute('app_topic', ['id' => $message->getTopic()->getId()]);
        }
        
        $formulaireMessage = $this->createForm(MessageType::class, $message);
        $formulaireMessage->handleRequest($request);

        if($formulaireMessage->isSubmitted() && $formulaireMessage->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_topic', ['id' => $message->getTopic()->getId()]);
        }

        return $this->render('message/edit.html.twig', [
            'message' => $message,
            'vueFormulaireMessage' => $formulaireMessage->createView()
        ]);
    }
}
