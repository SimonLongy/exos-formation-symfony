<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\Topic;
use App\Form\MessageType;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TopicController extends AbstractController
{
    #[Route('/topic/{id}', name: 'app_topic')]
    public function showTopic(Request $request, Topic $topic, MessageRepository $messageRepository, EntityManagerInterface $em): Response
    {
        if($topic->getPrive() && !$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }

        // Création d'un message initialement vierge
        $message = new Message();

        // Création d'un objet formulaire pour ajouter un message
        $formulaireMessage = $this->createForm(MessageType::class, $message);

        // Récupération des données dans $message si elles ont été soumises
        $formulaireMessage->handleRequest($request);

        // Traiter les données du formulaire s'il a été soumis et est valide
        if($formulaireMessage->isSubmitted() && $formulaireMessage->isValid())
        {
            // Set le topic du message
            $message->setTopic($topic);

            // Les dates de création et de modification sont faites avec l'event Persist
            $em->persist($message);
            $em->flush();

            // Rediriger l'utilisateur vers la page du topic
            return $this->redirectToRoute('app_topic', ['id' => $topic->getId()]);
        }

        $messages = $messageRepository->findByTopicAntechronologique($topic);

        return $this->render('topic/topic.html.twig', [
            'topic' => $topic,
            'messages' => $messages,
            'vueFormulaireMessage' => $formulaireMessage->createView()
        ]);
    }
}
