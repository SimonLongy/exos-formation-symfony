<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use App\Entity\Message;
use App\Entity\Topic;
use App\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\Service\Attribute\Required;

class AppFixtures extends Fixture
{
    #[Required]
    public UserPasswordHasherInterface $passwordHasher;

    public function load(ObjectManager $manager): void
    {
        $users = [];
        $users[] = $admin = new Utilisateur();
        $admin
            ->setUsername('admin')
            ->setPassword($this->passwordHasher->hashPassword($admin, 'admin'))
            ->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);

        for ($y = 1; $y <= 3; $y++) {
            $users[] = $utilY = (new Utilisateur())
                ->setUsername("user$y")
                ->setPassword($this->passwordHasher->hashPassword($admin, "user$y"));
            $manager->persist($utilY);
        }

        /** @var array<Categorie> $categories */
        $categories = [];

        for ($x = 1; $x <= 3; $x++) {
            $categories[] = (new Categorie())
                ->setTitre("Catégorie $x")
                ->setDescription("Topics relatifs à la catégorie $x");
        }

        $categories[] = $sousCategorie1 = (new Categorie())
            ->setTitre('Sous-catégorie 1-1')
            ->setDescription('Topics relatifs à la catégorie 1-1');
        $categories[] = $sousCategorie2 = (new Categorie())
            ->setTitre('Sous-catégorie 1-2')
            ->setDescription('Topics relatifs à la catégorie 1-2');
        $categories[] = $sousCategorie3 = (new Categorie())
            ->setTitre('Sous-catégorie 3-1')
            ->setDescription('Topics relatifs à la catégorie 3-1');

        $categories[0]->addCategoriesFille($sousCategorie1)->addCategoriesFille($sousCategorie2);
        $categories[2]->addCategoriesFille($sousCategorie3);

        foreach ($categories as $categorie) {
            $categorie->setCreateur($admin);
            $manager->persist($categorie);

            for ($i = 0; $i < 5; $i++) {
                $topic = (new Topic())
                    ->setCategorie($categorie)
                    ->setTitre("Topic n°$i de la {$categorie->getTitre()}");

                $categorie->addTopic($topic);
                $topic
                    ->setCreateur($users[array_rand($users)])
                    // On donne plus de chance de faire
                    // des topics publics que des topics privés
                    ->setPrive(random_int(0, 2) % 2 === 1)
                ;
                $manager->persist($topic);
                for ($j = 1; $j <= 10; $j++) {
                    $message = (new Message())
                        ->setTopic($topic)
                        ->setContenu("Message n°$j du {$topic->getTitre()}");
                    $topic->addMessage($message);
                    $message->setCreateur($users[array_rand($users)]);
                    $manager->persist($message);
                }
            }
        }

        $manager->flush();
    }
}
