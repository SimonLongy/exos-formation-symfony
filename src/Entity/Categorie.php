<?php

namespace App\Entity;

use App\Repository\CategorieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategorieRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Categorie
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    private $titre;

    #[ORM\Column(type: 'string', length: 255)]
    private $description;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'categoriesFilles')]
    private $parent;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: self::class)]
    private $categoriesFilles;

    #[ORM\OneToMany(mappedBy: 'categorie', targetEntity: Topic::class)]
    private $topics;

    #[ORM\ManyToOne(targetEntity: Utilisateur::class, inversedBy: 'categories')]
    #[ORM\JoinColumn(nullable: false)]
    private $createur;

    public function __construct()
    {
        $this->categoriesFilles = new ArrayCollection();
        $this->topics = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getCategoriesFilles(): Collection
    {
        return $this->categoriesFilles;
    }

    public function addCategoriesFille(self $categoriesFille): self
    {
        if (!$this->categoriesFilles->contains($categoriesFille)) {
            $this->categoriesFilles[] = $categoriesFille;
            $categoriesFille->setParent($this);
        }

        return $this;
    }

    public function removeCategoriesFille(self $categoriesFille): self
    {
        if ($this->categoriesFilles->removeElement($categoriesFille)) {
            // set the owning side to null (unless already changed)
            if ($categoriesFille->getParent() === $this) {
                $categoriesFille->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Topic>
     */
    public function getTopics(): Collection
    {
        return $this->topics;
    }

    public function addTopic(Topic $topic): self
    {
        if (!$this->topics->contains($topic)) {
            $this->topics[] = $topic;
            $topic->setCategorie($this);
        }

        return $this;
    }

    public function removeTopic(Topic $topic): self
    {
        if ($this->topics->removeElement($topic)) {
            // set the owning side to null (unless already changed)
            if ($topic->getCategorie() === $this) {
                $topic->setCategorie(null);
            }
        }

        return $this;
    }

    public function getCreateur(): ?Utilisateur
    {
        return $this->createur;
    }

    public function setCreateur(?Utilisateur $createur): self
    {
        $this->createur = $createur;

        return $this;
    }
}
