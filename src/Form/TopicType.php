<?php

namespace App\Form;

use App\Entity\Topic;
use App\Validator\BlacklistConstraint;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

class TopicType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new Length(null, 1, 50)
                ]
            ])
            ->add('contenuPremierMessage', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new BlacklistConstraint()
                ],
                'mapped' => false
            ])
            ->add('boutonSoumission', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Topic::class,
        ]);
    }
}
