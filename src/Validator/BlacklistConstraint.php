<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
* @Annotation
*/
class BlacklistConstraint extends Constraint
{
    public $message = 'La chaîne "{{ string }}" contient un mot interdit...';

    public function validatedBy()
    {
        return static::class.'Validator';
    }
}